<?php

class Regions extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    

    public function addRegion($data){
        $data = array
        (
            'r_name' => $data['region_name'],
        );
        if($this->db->insert('region',$data)){
            return TRUE;
        }    
        return FALSE;
    }

    public function viewAll(){
        $query = $this->db->get('region');
        return $query->result();          
    }
    
    public function viewRegion($id){
        $this->db->select('*');
        $this->db->from('region');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    
    public function regionUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'r_name' => $data['region_name'],
        );
        $this->db->where('id', $id);
        $this->db->update('region',$data);          
    }
   
    public function delete($id){
        $this->db->delete('region', array('id' => $id));
        // delete country
        $this->db->delete('country', array('region_id' => $id));
        /// delete package
        $data = array
        (
            'deleted' => 1,
        );
        $this->db->where('region_id', $id);
        $this->db->update('package',$data);         
    }      
}

