<?php

class Hotels extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    

    public function addHotel($data){
        $data = array
        (
            'hotel_name' => $data['hotel_name'],
            'hotel_description' => $data['hotel_description'],
        );
        if($this->db->insert('hotel',$data)){
            return TRUE;
        }    
        return FALSE;
    }
    
    public function viewHotel($id){
        $this->db->select('*');
        $this->db->from('hotel');
        $this->db->where('hotel_id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    
    public function viewAll(){
        $query = $this->db->get('hotel');
        return $query->result();       
    }    
    
    public function delete($id){
        $this->db->delete('hotel', array('hotel_id' => $id)); 
    } 
   
    public function hotelUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'hotel_name' => $data['hotel_name'],
            'hotel_description' => $data['hotel_description'],
        );
        $this->db->where('hotel_id', $id);
        $this->db->update('hotel',$data);          
    }
    
    public function viewGallery($hotelId){
        $querySet = " 
            SELECT hotels_gallery.*
            FROM hotels_gallery
            WHERE hotels_gallery.hotel_id='".$hotelId."' 
        ";
        $query = $this->db->query($querySet);
        return $query->result();         
    }    
    
    public function addGallery($data){
        $data = array
        (
            'hotel_id' => $data['hotel_id'],
            'file_name' => $data['file_name'],
            'thumb_image' => $data['thumb_image'],
        );
        if($this->db->insert('hotels_gallery',$data)){
            return 1;
        }else{
            return 0;
        }         
    }
    
    public function imgData($id){
        $querySet = " 
            SELECT hotels_gallery.*
            FROM hotels_gallery
            WHERE hotels_gallery.image_id='".$id."'
        ";
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    public function delImg($id){
        $this->db->delete('hotels_gallery', array('image_id' => $id)); 
    }  
    
}

