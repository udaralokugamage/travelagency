<?php

class Users extends CI_Model {

    protected  $user_type;
    protected $user_id;
    public function __construct() {
            parent::__construct();
        // user type
        $this->user_type = $this->session->userdata('user_type');
        $this->user_id = $this->session->userdata('id');            
    }

    public function userLogin($data){
        // get encrypted password
        $getPassword = $this->passwordGenerate($data['password']);
        // select user
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_email', $data['email']);
        $this->db->where('user_password', $getPassword);
        $this->db->where('user_status', '1');
        $query = $this->db->get();
        if ($query->num_rows()>0){
            $row = $query->row_array();
            return $row;
        }
    }
    
    public function userRegister($data){
        $password = $this->passwordGenerate($data['password']);
        $created_date = date("Y-m-d H:i:s");
        $status = 1; 
       
        $data = array
        (
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'user_email' => $data['email'],
            'user_password' => $password,
            'user_type' => $data['user_type'],
            'user_status' => $status,
            'created_date' => $created_date
        );
        $this->db->insert('user',$data);        
    }
    
    /**
     * 
     * @param type $password
     * @return encripted password
     */
    public function passwordGenerate($password){
        // convert password to sha512
        $step = hash('sha512', $password, false);
        return $step;
    }
    
    public function viewAll(){
        $querySet = '
            SELECT *
            FROM user
        ';
        if($this->user_type == 2){
            $querySet .= " WHERE user_id='".$this->user_id."'";
        }
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    public function viewUser($id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        return $query->result();        
    }

    public function userUpdate($data){
        $updated_date = date("Y-m-d H:i:s");
        $id = $data['id'];
        $data = array
        (
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'user_type' => $data['user_type'],
            'updated_date' => $updated_date
        );
        $this->db->where('user_id', $id);
        $this->db->update('user',$data);             
    }
  
    public function delete($id){
        $this->db->delete('user', array('user_id' => $id)); 
    }  
    
    public function userPasswordUpdate($data){
        $id = $data['id'];
        $password = $this->passwordGenerate($data['new_password']);
        $updated_date = date("Y-m-d H:i:s");
        $data = array
        (
            'user_password' => $password,
            'updated_date' => $updated_date
        );
        $this->db->where('user_id', $id);
        $this->db->update('user',$data);         
    }
    
    public function changeStatus($status, $cus_id){
        $data = array
        (
            'user_status' => $status,
        );
        $this->db->where('user_id', $cus_id);
        $this->db->update('user',$data);         
    } 
    
    public function userForgotPassword($data){
        // select user
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_email', $data['email']);
        $query = $this->db->get();
        if ($query->num_rows()>0){
            $row = $query->row_array();
            return $row;
        }        
    }
    
    public function addQueryString($email, $link){
        $updated_date = date("Y-m-d H:i:s");
        $data = array
        (
            'change_password_code' => $link,
            'updated_date' => $updated_date
        );
        $this->db->where('user_email', $email);
        $this->db->update('user',$data);        
    }
    
    public function userResetPassword($password, $link){
        $password = $this->passwordGenerate($password);
        $updated_date = date("Y-m-d H:i:s");
        $data = array
        (
            'user_password' => $password,
            'change_password_code' => '',
            'updated_date' => $updated_date
        );
        $this->db->where('change_password_code', $link);
        $this->db->update('user',$data);
        if($this->db->affected_rows()>0){
            return 1;
        }else{
            return 0;
        }
    }
    
}
