<?php

class Countries extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    

    public function addCountry($data){
        $data = array
        (
            'name' => $data['country_name'],
            'region_id' => $data['region_id'],
        );
        if($this->db->insert('country',$data)){
            return TRUE;
        }    
        return FALSE;
    }

    public function viewAll(){
        $querySet = '
            SELECT country.*, country.name as country_name, region.r_name as region_name
            FROM country
            JOIN region ON region.id = country.region_id
        ';
        $query = $this->db->query($querySet);
        return $query->result();           
    }
    
    public function viewCountry($id){
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    
    public function countryUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'name' => $data['country_name'],
            'region_id' => $data['region_id'],
        );
        $this->db->where('id', $id);
        $this->db->update('country',$data);          
    }
   
    public function delete($id){
        // country delete
        $this->db->delete('country', array('id' => $id)); 
        // delete packages link to country
        $data = array
        (
            'deleted' => 1,
        );
        $this->db->where('country_id', $id);
        $this->db->update('package',$data);         
    }   
    
    public function regionCountry($region_id){
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('region_id', $region_id);
        $query = $this->db->get();
        return $query->result();         
    }
}

