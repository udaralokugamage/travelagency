<?php

class Categories extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function parentCategories() {
        $query = $this->db->get('category');
        return $query->result();
    }

    public function addCategory($data) {
        if ($data['parent_category'] == 0) {
            $data = array
                (
                'category_name' => $data['category_name'],
            );
            if ($this->db->insert('category', $data)) {
                return TRUE;
            }
        } else {
            $data = array
                (
                'subcategory_name' => $data['category_name'],
                'category_id' => $data['parent_category']
            );
            if ($this->db->insert('subcategory', $data)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function updateCategory($data) {
        $id = $data['id'];
        if ($data['parent_category'] == 0) {
            $data = array
                (
                'category_name' => $data['category_name'],
            );
            $this->db->where('category_id', $id);
            if ($this->db->update('category', $data)) {
                return TRUE;
            }
        } else {
            $data = array
                (
                'subcategory_name' => $data['category_name'],
                'category_id' => $data['parent_category']
            );
            $this->db->where('subcategory_id', $id);
            if ($this->db->update('subcategory', $data)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function subCategories($cat_id) {
        $query = $this->db->get_where('subcategory', array('category_id' => $cat_id));
        return $query->result();
    }

    public function getAllCategories($id = NULL) {
        $querySet = '
            SELECT *
            FROM category
        ';
        if (isset($id)) {
            $querySet .= " WHERE category_id='" . $id . "'";
        }

        $query = $this->db->query($querySet);
        return $query->result();
    }

    public function getAllSubcategories($id = NULL) {
        $querySet = '
            SELECT subcategory.*,category.category_name
            FROM category
            JOIN subcategory ON category.category_id = subcategory.category_id
        ';
        if (isset($id)) {
            $querySet .= " WHERE subcategory_id='" . $id . "'";
        }

        $query = $this->db->query($querySet);
        return $query->result();
    }

    public function deleteCategories($type, $cat_id) {
        /*
         * if it is a category
         * delete that category
         * delete all subcategories belong to it
         * delete all packages link to those subcategories
         */
        if ($type == "cat") {
            $this->db->delete('category', array('category_id' => $cat_id));
            
            // delete packages
            $querySet = " 
                SELECT subcategory_id
                FROM subcategory
                WHERE category_id='".$cat_id."'
            ";
            $query = $this->db->query($querySet);
            $results = $query->result();
            if(!empty($results)){
                foreach($results as $res){
                    $this->db->delete('package', array('subcategory_id' => $res->subcategory_id));
                }
            }
            // delete subcategories
            $this->db->delete('subcategory', array('category_id' => $cat_id));
        }
        /*
         * if it is subcategory
         * delete that subcategory
         * delete all packages link to that subcategory
         */
        if ($type == "sub_cat") {
            // delete packages
            $this->db->delete('package', array('subcategory_id' => $cat_id));
            // delete subcategories
            $this->db->delete('subcategory', array('subcategory_id' => $cat_id));            
        }
    }

}
