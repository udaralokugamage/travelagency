<?php

class Orders extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
    public function viewAll($userType, $userId, $limit = NULL){
        $querySet = " 
            SELECT package.*, CONCAT(customer.fname,' ',customer.lname) AS customer_name , customer.email AS customer_email, DATE(booking.date_added) AS orderdate , booking.*
            FROM booking
            JOIN customer ON customer.id = booking.cus_id
            JOIN package ON package.package_id = booking.package_id            
        ";
        
        if($userType == 2){
            $querySet .= " WHERE package.created_by='".$userId."'";
        }
        
        if(isset($limit)){
            $querySet .= " ORDER BY booking.bid DESC LIMIT ".$limit;
        }

        
        $query = $this->db->query($querySet);
        return $query->result();           
    } 
    
    
    public function updateOrder($orderId, $status){
        $data = array
        (
            'status' => $status,
        );
        $this->db->where('bid', $orderId);
        if($this->db->update('booking',$data)){
            return 1;
        }else{
            return 0;
        }        
    }
    
    public function pendingOrders($userType, $userId){
        $querySet = " 
            SELECT COUNT(*) as pending_orders
            FROM booking
        ";
        // for agents
        if($userType == 2){
            $querySet .= "
                JOIN package ON package.package_id = booking.package_id
            ";
        }
        $querySet .= "WHERE booking.status='0'";
        // for agents
        if($userType == 2){
            $querySet .= " AND package.created_by='".$userId."'";
        }
        
        $query = $this->db->query($querySet);
        return $query->result();         
    }
}
?>