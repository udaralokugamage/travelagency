<?php

class Packages extends CI_Model {

    protected  $user_type;
    protected $user_id;    
    public function __construct() {
            parent::__construct();
        // user type
        $this->user_type = $this->session->userdata('user_type');
        $this->user_id = $this->session->userdata('id');              
    }
    
    public function addPackage($data){
        
        $created_date = date("Y-m-d H:i:s");
        // get user type
        $userType = $this->session->userdata('user_type');
        if($userType == 1){
            $status = 1;
        }else{
            $status = 0;
        }
        // created user
        $createdBy = $this->session->userdata('id');
        
        $data = array
        (
            'package_name' => $data['package_name'],
            'fixed_no_of_days' => $data['fixed_no_of_days'],
            'description' => $data['description'],
            'detailed_itinerary' => $data['detailed_itinerary'],
            'price_adult' => $data['price_adult'],
            'price_child' => $data['price_child'],
            'thumb_image' => $data['thumb_image'],
            'banner_image' => $data['banner_image'],
            'status' => $status,
            'created_by' => $createdBy,
            'created_date' => $created_date,
            'region_id'=>$data['region_id'],
            'region'=>$data['region_name'],
            'country_id'=>$data['country_id'],
            'country'=>$data['country_name'],
        );
        if($this->db->insert('package',$data)){
            return 1;
        }else{
            return 0;
        }        
    }
    
    public function viewAll($user_id = NULL){
        $querySet = '
            SELECT package.*, country.region_id, CONCAT(user.first_name," ",user.last_name) AS created_user, 
                (
                    SELECT CONCAT(region.r_name," > ",country.name) 	
                    FROM country
                    JOIN region ON country.region_id = region.id
                    WHERE country.id = package.country_id
                ) AS category_name,
                (case when (status = 1) 
                    THEN
                         "Enable" 
                    ELSE
                         "Disable" 
                    END)
                    as status_text 
            FROM package
            JOIN user ON user.user_id = package.created_by
            JOIN country ON country.id = package.country_id  
            WHERE package.deleted=0
        ';
        if(!empty($user_id)){
            $querySet .= " AND package.created_by='".$user_id."'";
        }
        
        $query = $this->db->query($querySet);
        return $query->result();        
    }
    
    public function getPackage($packageId){
        $querySet = '
            SELECT package.*, country.id as country_id
            FROM package
            JOIN user ON user.user_id = package.created_by
            JOIN country ON country.id = package.country_id   
            WHERE package_id="'.$packageId.'"
        ';
        if($this->user_type == 2){
            $querySet .= " AND package.created_by='".$this->user_id."'";
        }
        
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    public function updatePackage($data){
        $updated_date = date("Y-m-d H:i:s");
        $id = $data['package_id'];
        $data = array
        (
            'package_name' => $data['package_name'],
            'fixed_no_of_days' => $data['fixed_no_of_days'],
            'description' => $data['description'],
            'detailed_itinerary' => $data['detailed_itinerary'],
            'price_adult' => $data['price_adult'],
            'price_child' => $data['price_child'],
            'thumb_image' => $data['thumb_image'],
            'banner_image' => $data['banner_image'],
            'updated_date' => $updated_date,
            'region_id'=>$data['region_id'],
            'region'=>$data['region_name'],
            'country_id'=>$data['country_id'],
            'country'=>$data['country_name']            
        );
        $this->db->where('package_id', $id);
        if($this->db->update('package',$data)){
            return 1;
        }else{
            return 0;
        }
    }     

    public function delete($id){
        $data = array
        (
            'deleted' => 1,
        );
        $this->db->where('package_id', $id);
        $this->db->update('package',$data);          
    }    
 
    public function viewGallery($createdBy, $packageId){
        $querySet = " 
            SELECT gallery.*
            FROM gallery
            JOIN package ON package.package_id = gallery.package_id
            WHERE gallery.package_id='".$packageId."' ";
        $userType = $this->session->userdata('user_type');
        if($userType == 1){
            $querySet .= "";
        }else{
            $querySet .= " AND package.created_by='".$createdBy."'";
        }        

        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    public function addGallery($data){
        $data = array
        (
            'package_id' => $data['package_id'],
            'file_name' => $data['file_name'],
            'thumb_image' => $data['thumb_image'],
        );
        if($this->db->insert('gallery',$data)){
            return 1;
        }else{
            return 0;
        }         
    }
    
    public function imgData($id){
        $querySet = " 
            SELECT gallery.*
            FROM gallery
            WHERE gallery.image_id='".$id."'
        ";
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    public function delImg($id){
        $this->db->delete('gallery', array('image_id' => $id)); 
    }
    
    public function agentPendingPackages(){
        $querySet = " 
            SELECT COUNT(*) as agent_packages
            FROM package
            JOIN user ON user.user_id = package.created_by
            WHERE user.user_type='2'  AND package.status='0' AND package.status_change_track='0' AND deleted=0       
        ";
        $query = $this->db->query($querySet);
        return $query->result();          
    }
    
    public function approvePackages(){
        $querySet = '
            SELECT package.*, country.region_id, CONCAT(user.first_name," ",user.last_name) AS created_user, 
                (
                    SELECT CONCAT(region.r_name," > ",country.name) 	
                    FROM country
                    JOIN region ON country.region_id = region.id
                    WHERE country.id = package.country_id
                ) AS category_name
            FROM package
            JOIN user ON user.user_id = package.created_by
            JOIN country ON country.id = package.country_id  
            WHERE user.user_type="2" AND deleted=0
            ORDER BY FIELD(status, 0, 1)
        ';
        $query = $this->db->query($querySet);
        return $query->result();        
    }   
    
    public function changeStatus($status, $cus_id){
        $data = array
        (
            'status' => $status,
            'status_change_track' => 1
        );
        $this->db->where('package_id', $cus_id);
        $this->db->update('package',$data);         
    }    
}
