<?php
/**
 * this class not used. categories replace with region and country
 */ 
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
    }    
    
    public function index(){
        $this->load->model('Categories');
        $this->load->helper(array('form', 'url'));
        $this->data['allCategories'] = $this->Categories->getAllCategories();
        $this->data['allSubcategories'] = $this->Categories->getAllSubcategories();
        $this->load->template('tmp_view_category', $this->data);
    }
    
    public function add(){
        $this->load->model('Categories');
        $this->data['parentCategories'] = $this->Categories->parentCategories();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_add_category', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $data['category_name'] = $this->input->post('category_name');
                $data['parent_category'] = $this->input->post('parent_category');
                // save data on table
                if($this->Categories->addCategory($data)){
                    $this->data['msg'] = 'Category Added';
                    $this->data['parentCategories'] = $this->Categories->parentCategories();
                    $this->load->template('tmp_add_category', $this->data);
                }else{
                    $this->data['msg'] = 'Failed to add category';
                    $this->load->template('tmp_add_category',  $this->data);
                }
            }                
        }         
    }
    
    
    public function edit(){
        $this->load->model('Categories');
        $this->data['parentCategories'] = $this->Categories->parentCategories();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');
        
        if(isset($_GET['cat_id'])){
            $this->data['catData'] = $this->Categories->getAllCategories($_GET['cat_id']);
        }
        if(isset($_GET['sub_cat_id'])){
            $this->data['catData'] = $this->Categories->getAllSubcategories($_GET['sub_cat_id']);
        }        

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_edit_category', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $data['category_name'] = $this->input->post('category_name');
                $data['parent_category'] = $this->input->post('parent_category');
                if(isset($_GET['cat_id'])){
                    $data['id'] = $_GET['cat_id'];
                }else{
                    $data['id'] = $_GET['sub_cat_id'];
                }
                // save data on table
                if($this->Categories->updateCategory($data)){
                    if(isset($_GET['cat_id'])){
                        $this->data['catData'] = $this->Categories->getAllCategories($_GET['cat_id']);
                    }
                    if(isset($_GET['sub_cat_id'])){
                        $this->data['catData'] = $this->Categories->getAllSubcategories($_GET['sub_cat_id']);
                    }                     
                    $this->data['msg'] = 'Category Updated';
                    $this->data['parentCategories'] = $this->Categories->parentCategories();
                    $this->load->template('tmp_edit_category', $this->data);
                }else{
                    $this->data['msg'] = 'Failed to update category';
                    $this->load->template('tmp_edit_category',  $this->data);
                }
            }                
        }         
    }
    
    public function delete(){
        $this->load->model('Categories');
        if(isset($_GET['cat_id'])){
            $this->Categories->deleteCategories('cat',$_GET['cat_id']);
        }
        if(isset($_GET['subcat_id'])){
            $this->Categories->deleteCategories('sub_cat',$_GET['subcat_id']);
        }       
        redirect('/category');
    }

}