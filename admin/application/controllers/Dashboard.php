<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_logged_in()) {
            redirect('/');
        }
        $this->load->helper(array('form', 'url'));
    }

    public function index() {
        // load models
        $this->load->model('Orders');
        $this->load->model('Packages');
        // session data
        $data['first_name'] = $this->session->userdata('first_name');
        $data['last_name'] = $this->session->userdata('last_name');
        $data['user_type'] = $this->session->userdata('user_type');
        $data['user_id'] = $this->session->userdata('id');
        // get pending bookings
        // if status not changed, then it is considered as pending booking
        // for admin users show all pending bookings
        // for agent users show bookings related to agents packages
        $data['count_orders'] = $this->Orders->pendingOrders($data['user_type'], $data['user_id']);
        // list latest bookings
        // for agent users show bookings related to agents packages
        $allOrders = $this->Orders->viewAll($data['user_type'], $data['user_id'],5);
        $data['latest_orders'] = array();
        if(!empty($allOrders)){
            foreach($allOrders as $key=>$order){
                $data['latest_orders'][$key]['order_details'] = $order->package_name." &minus; ".$order->customer_name;
                $data['latest_orders'][$key]['time_ago'] = $this->time_elapsed_string($order->date_added);
            }
        }
        // if an agent add a package
        // admin should have to enable that package
        $data['pending_packages'] = $this->Packages->agentPendingPackages();
        $this->load->template('tmp_dashboard', $data);
    }

    public function logout() {
        // destroy session for logout process
        session_destroy();
        redirect('/');
    }

    /**
     * used https://stackoverflow.com/questions/1416697/converting-timestamp-to-time-ago-in-php-e-g-1-day-ago-2-days-ago/37943300
     * @param type $datetime
     * @param type $full
     * @return ago time
     */
    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

}
