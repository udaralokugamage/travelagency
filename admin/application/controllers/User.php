<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    public $data = array();
    public $userTypes = array(1=>'Super Admin', 2=>'Agent');
    
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        // session data
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        $this->data['userTypes'] = $this->userTypes;
        // user type and id in session
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');        
    }

    public function index(){
        // list all users
        $this->load->model('Users');
        $this->load->helper(array('form', 'url'));
        $this->data['allData'] = $this->Users->viewAll();
        $this->load->template('tmp_view_user', $this->data);        
    }
    
    public function add(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller         
        if($this->data['user_type'] == 2){
           redirect('/access');
        }
        $this->load->model('Users');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('last_name', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.user_email]');                
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_registration_page', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $data['first_name'] = $this->input->post('first_name');
                $data['last_name'] = $this->input->post('last_name');
                $data['email'] = $this->input->post('email');
                $data['password'] = $this->input->post('password');
                $data['user_type'] = $this->input->post('user_type');
                // save data on table
                $this->Users->userRegister($data);
                $this->data['msg'] =  'User Added';
                $this->load->template('tmp_registration_page', $this->data);
            }                
        }         
    }

    public function edit(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller         
        if($this->data['user_type'] == 2){
           redirect('/access');
        }        
        $this->load->model('Users');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->data['userData'] = $this->Users->viewUser($_GET['id']);

        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('last_name', 'Last name', 'required');

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_edit_user', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $data['first_name'] = $this->input->post('first_name');
                $data['last_name'] = $this->input->post('last_name');
                $data['user_type'] = $this->input->post('user_type');
                // user id
                $data['id'] = $_GET['id'];
                // save data on table
                $this->Users->userUpdate($data);
                $this->data['msg'] = 'User Updated';
                $this->data['userData'] = $this->Users->viewUser($_GET['id']);
                $this->load->template('tmp_edit_user', $this->data);
            }                
        }         
    }

    public function delete(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller         
        if($this->data['user_type'] == 2){
           redirect('/access');
        }        
        $this->load->model('Users');
        $this->Users->delete($_GET['id']);
        redirect('/user');
    }
    
    public function changePassword(){
        $this->load->model('Users');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        //session id
        $user_id = $this->session->userdata('id');
        $userData = $this->Users->viewUser($user_id);

        $this->form_validation->set_rules('old_password', 'Old Password', 'required');
        $this->form_validation->set_rules('new_password','New Password','required|min_length[6]');
        $this->form_validation->set_rules('confirm_new_password','Confirm Password','required|matches[new_password]');

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_change_password', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $enteredOldPassword = $this->Users->passwordGenerate($this->input->post('old_password'));
                if($userData[0]->user_password == $enteredOldPassword){
                    $data['new_password'] = $this->input->post('new_password');
                    $data['id'] = $user_id;
                    // save data on table
                    $this->Users->userPasswordUpdate($data);
                    $this->data['msg'] = 'Password Updated';                    
                }else{
                    $this->data['msg'] = 'Incorrect Old Password';
                }

                $this->load->template('tmp_change_password', $this->data);
            }                
        }         
    }
    
    public function status(){
        // user status change
        $this->load->model('Users');
        $this->Users->changeStatus($_GET['st'], $_POST['user_id']);
    }    
    
}