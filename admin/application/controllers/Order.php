<?php
/**
 * booking class
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        // session data
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
    }    
    
    /**
     * view all bookings
     */
    public function index(){
        // session data
        $data['user_type'] = $this->session->userdata('user_type');
        $data['user_id'] = $this->session->userdata('id');
        // load models
        $this->load->model('Orders');
        // load helper forms and urls eg. base_url();
        $this->load->helper(array('form', 'url'));
        // update status
        // events pendings, shipped, cancel
        // if press update button
        if(isset($_POST['update'])){
            // if select a status from dropdown
            if(isset($_POST['status'])){
                // if select atleast one booking
                if(isset($_POST['order_status'])){
                    // loop order status array. this array contain booking ids.
                    foreach($_POST['order_status'] as $id){
                        // update orders
                        $this->Orders->updateOrder($id, $_POST['status']);
                    }                        
                }
            }
        }        
        // get all bookings
        // admin users can view all bookings
        // agent users can view only bookings related to their packages
        $this->data['allData'] = $this->Orders->viewAll($data['user_type'], $data['user_id']);
        $this->load->template('tmp_view_orders', $this->data); 
    }
}
?>