<?php
/**
 * Restricted users redirect to this class
 */ 
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

	public function index()
	{
		$this->load->view('errors/html/error_forbidden');
	}
}