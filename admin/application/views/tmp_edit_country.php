<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Edit Country</h1>
                    
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                
                    <form action="" method="post">
                        <div class="form-group required"> 
                           <label class="control-label">Country name:</label> 
                             <input class="form-control" type="text" name="country_name" value="<?php echo $countryData[0]->name ?>" />
                             <?php echo form_error('country_name'); ?>                       
                        </div>
                       <div class="form-group required"> 
                             <label class="control-label">Region:</label>
                             <select name="region_id" class="form-control">
                                 <option value="">Select a Region</option>
                                 <?php if(!empty($allRegions)){ ?>
                                    <?php foreach($allRegions as $region){ ?>
                                        <?php
                                            $select = "";
                                            if($countryData[0]->region_id == $region->id){
                                                $select = ' selected="selected" ';
                                            }
                                        ?>
                                        <option <?php echo $select; ?> value="<?php echo $region->id; ?>"><?php echo $region->r_name; ?></option>
                                    <?php } ?>
                                 <?php } ?>
                             </select>
                             <?php echo form_error('region_id'); ?>
                        </div>                         
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
