<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
            
            <div class="col-lg-12">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-shopping-cart fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $count_orders[0]->pending_orders; ?></div>
                                <div>New Orders!</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo base_url() ?>order">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <?php if($user_type == 1){ ?>    
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $pending_packages[0]->agent_packages; ?></div>
                                <div>New Packages</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo base_url() ?>package/approve">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div> 
            <?php } ?>    
            </div>

            <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bell fa-fw"></i> Latest Bookings
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="list-group">
                        <?php if(!empty($latest_orders)){ ?>
                            <?php foreach($latest_orders as $data){ ?>                        
                                <a href="<?php echo base_url() ?>/order" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> <?php echo $data['order_details']; ?>
                                    <span class="pull-right text-muted small"><em><?php echo $data['time_ago']; ?></em></span>
                                </a>
                            <?php } ?>
                        <?php } ?>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>     
            </div>


        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
