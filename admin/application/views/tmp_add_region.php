<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add Region</h1>
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post">
                       <div class="form-group required"> 
                             <label class="control-label">Region name:</label>
                             <input class="form-control" type="text" name="region_name" />
                             <?php echo form_error('region_name'); ?>
                        </div>
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add Region"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
