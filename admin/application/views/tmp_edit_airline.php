<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Edit Airline</h1>
                    
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                
                    <form action="" method="post" enctype="multipart/form-data">
                       <div class="form-group required"> 
                             <label class="control-label">Airline name:</label>
                             <input class="form-control" type="text" name="airline_name" value="<?php echo $airlineData[0]->airline_name ?>" />
                             <?php echo form_error('airline_name'); ?>
                        </div>
                       <div class="form-group"> 
                             <label>Description:</label>
                             <input class="form-control" type="text" name="airline_description" value="<?php echo $airlineData[0]->airline_description ?>" />
                             <?php echo form_error('airline_description'); ?>
                        </div>
                       <div class="form-group">
                           <label>Thumbnail Image:</label>
                            <br/> 
                           <?php if(!empty($airlineData[0]->thumb_image)){ ?>
                           <img src="<?php echo base_url().'uploads/airline/'.$airlineData[0]->thumb_image; ?>"/>
                           <?php } ?>
                           <br/><br/>                           
                           <input type="file" name="thumb_image"/>
                        </div>                        
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
