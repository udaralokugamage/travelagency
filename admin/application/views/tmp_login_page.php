<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Administrator - Travel Agency </title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <p>&nbsp;</p>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Travel Agency Admin Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <?php if (isset($msg)) { ?>
                                <div class="alert alert-warning alert-dismissable">
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>    
                            <?php if (isset($_GET['msg'])) { ?>
                                <div class="alert alert-warning alert-dismissable">
                                    Password has been reset
                                </div>
                            <?php } ?>                             
                            <form action="" method="post" role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <?php echo form_error('user_email'); ?>
                                        <input class="form-control" type="text" name="user_email" placeholder="E-mail" autofocus/>
                                    </div>
                                    <?php echo form_error('user_password'); ?>
                                    <div class="form-group">
                                        <input placeholder="Password" class="form-control" type="password" name="user_password"/>
                                    </div>    
                                    <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Sign in"/>
                                </fieldset>
                            </form> 
                            <br/>
                            <a href="<?php echo base_url(); ?>login/forgot">Having trouble logging in?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    </body>

</html>
