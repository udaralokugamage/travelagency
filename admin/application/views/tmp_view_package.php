<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Packages</h1>
            </div>
        </div>
                <div class="col-lg-12">
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Package Name</th>
                                        <th>Region > Country</th>
                                        <th>Number of Days</th>
                                        <th>Price Per Adult</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th class="sorter-false">&nbsp;</th>
                                        <th class="sorter-false">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($allData)){ $i = 1; ?>
                                    <?php foreach($allData as $data){ ?>
                                        <?php 
                                            if($i%2 == 1){
                                                $cl = "odd";
                                            }else{
                                                $cl = "even";
                                            }
                                        ?>
                                        <tr class="<?php echo $cl ?> gradeX">
                                            <td><?php echo $data->package_name; ?></td>
                                            <td><?php echo $data->category_name; ?></td>
                                            <td><?php echo $data->fixed_no_of_days; ?></td>
                                            <td>$<?php echo $data->price_adult; ?></td>
                                            <td>
                                                <?php 
                                                    if($data->status == 1){ 
                                                        $check = ' checked="" ';
                                                    }else{
                                                        $check = '';
                                                    }
                                                ?>
                                                <input <?php echo $check; ?> type="checkbox" class="statustrack" value="<?php echo $data->package_id; ?>" data-toggle="toggle" data-on="Enabled" data-off="Disabled">                                            
                                            </td>
                                            <td><?php echo $data->created_user; ?></td>
                                            <td class="center">
                                                <a href="<?php echo base_url() ?>package/view?id=<?php echo $data->package_id; ?>">
                                                  <span class="glyphicon glyphicon-pencil"></span>
                                                </a>                                               
                                            </td>
                                            <td class="center">
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="<?php echo $data->package_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title" id="myModalLabel">Delete Package</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Are you sure? the record will be permanently deleted
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <button type="button" class="btn btn-danger" onclick="deleteRecord('<?php echo $data->package_id; ?>')">Delete</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>                                                
                                                    <a data-toggle="modal" data-target="#<?php echo $data->package_id; ?>"  style="cursor: pointer;">
                                                      <span class="glyphicon glyphicon-remove"></span>
                                                    </a>   
                                            </td>
                                        </tr>
                                    <?php $i++; } ?>
                                <?php } ?>    
                                 </tbody>
                            </table>
                        </div>            
            <!-- /.col-lg-12 -->
                </div>
           </div>
         </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    jQuery.noConflict();
        jQuery('#dataTables-example').DataTable({
            responsive: true,
            columnDefs: [
               { orderable: false, targets: [-1,-2] }
            ]            
        });

        function deleteRecord(id){
            window.location = '<?php echo base_url() ?>package/delete?id='+id;
        }          
        
        jQuery('.statustrack').change(function() {
           var id = $(this).val();
           if($(this).prop('checked')){
                $.post("<?php echo base_url() ?>package/status?st=1", {pack_id: id}, function(result){
                });               
           }else{
                $.post("<?php echo base_url() ?>package/status?st=0", {pack_id: id}, function(result){
                });                 
           }
        });
    </script>
<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script>

</script>