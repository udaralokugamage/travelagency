<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add Airline</h1>
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post"  enctype="multipart/form-data">
                       <div class="form-group required"> 
                             <label class="control-label">Airline name:</label>
                             <input class="form-control" type="text" name="airline_name" />
                             <?php echo form_error('airline_name'); ?>
                        </div>
                       <div class="form-group"> 
                             <label>Description:</label>
                             <input class="form-control" type="text" name="airline_description" />
                             <?php echo form_error('airline_description'); ?>
                        </div>
                       <div class="form-group">
                           <label>Thumbnail Image:</label>
                           <input type="file" name="thumb_image"/>
                        </div>                        
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add Airline"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
