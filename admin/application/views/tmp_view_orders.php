<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Bookings</h1>
            </div>
                      <div class="panel-body">
                          <form action="" method="post">
                              <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <td colspan="7">
                                            <select class="form-control" name="status">
                                                  <option value="">Change Order Status</option>
                                                  <option value="0">Pending</option>
                                                  <option value="2">Cancel</option>
                                                  <option value="1">Success</option>
                                            </select> 
                                            <input class="form-control btn-primary" type="submit" name="update" value="Change Status"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Order Date</th>
                                        <th>Customer</th>
                                        <th>Customer Email</th>
                                        <th>Package Name</th>
                                        <th>Order Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($allData)){ $i = 1; ?>
                                    <?php foreach($allData as $data){ ?>
                                        <?php 
                                            if($i%2 == 1){
                                                $cl = "odd";
                                            }else{
                                                $cl = "even";
                                            }
                                        ?>
                                        <tr class="<?php echo $cl ?> gradeX">
                                            <td><input type="checkbox" name="order_status[]" value="<?php echo $data->bid; ?>"/></td>
                                            <td><?php echo $data->orderdate; ?></td>
                                            <td><?php echo $data->customer_name; ?></td>
                                            <td><?php echo $data->customer_email; ?></td>
                                            <td><a href="<?php echo base_url() ?>package/view?id=<?php echo $data->package_id ?>" target="blank"><?php echo $data->package_name; ?></a></td>
                                            <td>
                                                <?php
                                                    if($data->status == 1){
                                                        echo "Success";
                                                    }else if($data->status == 0){
                                                        echo "Pending";
                                                    }else{
                                                        echo "Cancel";
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <div class="modal fade" id="<?php echo $data->bid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="myModalLabel">Booking Details</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Number of adults: <?php echo $data->no_of_adults; ?> <br/>
                                                                Adults total: $<?php echo $data->adults_total; ?> <br/>
                                                                Number of children: <?php echo $data->no_of_children; ?> <br/>
                                                                Children total: $<?php echo $data->child_total; ?> <br/>
                                                                Start Date: <?php echo $data->start_date; ?> <br/>
                                                                Subtotal: $<?php echo $data->sub_total; ?>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>                                                
                                                <a data-toggle="modal" data-target="#<?php echo $data->bid; ?>"  style="cursor: pointer;">
                                                  More Details
                                                </a>                                                 
                                            </td>
                                        </tr>
                                    <?php $i++; } ?>
                                <?php } ?>    
                                 </tbody>
                            </table>
                          </form>
                        </div>            
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

<!-- /#page-wrapper -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        jQuery.noConflict();
        jQuery('#dataTables-example').DataTable({
            responsive: true,
            columnDefs: [
               { orderable: false, targets: [-1,-2] }
            ]            
        });
       
    </script>