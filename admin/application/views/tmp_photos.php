<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Manage Photo Galleries</h1>
                <?php if (isset($msg)) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <?php echo $msg; ?>
                    </div>
                <?php } ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group required col-lg-6"> 
                        <label class="control-label">Package:</label>
                        <i id="loading" class="fa fa-spinner fa-spin" style="font-size:24px; color: #337AB7; display: none;"></i>
                        <select id="package_id" name="package_id" class="form-control" >
                            <option value="">Select a Package</option>
                            <?php if (!empty($allData)) { ?>
                                <?php foreach ($allData as $data) { ?>
                                    <?php
                                    $select = "";
                                    if (isset($_GET['id'])) {
                                        if ($_GET['id'] == $data->package_id) {
                                            $select = ' selected="selected" ';
                                        }
                                    }
                                    ?>
                                    <option <?php echo $select; ?> value="<?php echo $data->package_id; ?>"><?php echo $data->package_name; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('package_id'); ?>
                    </div>

                    <?php if (isset($_GET['id'])) { ?>
                        <div class="form-group col-lg-12">
                            <label>Upload Photos</label>
                            <input type="file" name="banners[]" multiple=""/>
                        </div>
                        <div class="form-group col-lg-12"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Upload"/>
                        </div>                        
                    <?php } ?>
                </form>

                <?php if (isset($_GET['id'])) { ?>
                    <?php if (empty($packageGallery)) { ?>
                        <div class="alert alert-warning col-lg-12">
                            No Photos
                        </div>                            
                    <?php } else { ?>
                        <form id="trashForm" action="<?php echo base_url(); ?>package/deleteImg?id=<?php echo $_GET['id']; ?>" method="post">
                            <div class="form-group col-lg-12"> 
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <input type="checkbox" id="checkAll" name="selectAll" value="all"> 
                                        &nbsp;&nbsp;
                                        <button data-toggle="modal" data-target="#trashbox" type="submit" id="trashbtn" class="btn btn-default btn-sm">
                                            <span class="glyphicon glyphicon-trash"></span> Trash 
                                        </button>  
                                    </div>
                                    <div class="panel-body">
                                        <?php foreach ($packageGallery as $photo) { ?>
                                            <div class="imgborder"> 
                                                <input type="checkbox" name="deleteimg[]" value="<?php echo $photo->image_id; ?>"/> <br/>
                                                <a class="example-image-link" href="<?php echo base_url() . 'uploads/gallery/' . $photo->file_name; ?>" data-lightbox="example-set" data-title="">
                                                    <img class="example-image" src="<?php echo base_url() . 'uploads/gallery/' . $photo->thumb_image; ?>"/> 
                                                </a>
                                            </div>                         
                                        <?php } ?>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    <?php } ?>
                <?php } ?>

            </div>
        </div>
    </div>
</div>  
<!--- trash dialog box --->
<div class="modal fade" id="trashbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Photo(s)</h4>
            </div>
            <div class="modal-body">
               Do you want to delete the selected photo(s)?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" onclick="deleteImges()" >Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>
<script>
    $("#package_id").change(function () {
        $('#loading').show();
        var id = $("#package_id").val();
        if (id) {
            window.location = '<?php echo base_url(); ?>package/gallery?id=' + id;
        } else {
            window.location = '<?php echo base_url(); ?>package/gallery';
        }
    });
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    
    $("#trashbtn").click(function (event) {
        event.preventDefault();
        checked = $("input[type=checkbox]:checked").length;
        if(checked) {
        }else{
          alert("Please select an item");
          return false;
        }
    });
      
    function deleteImges(){
        $('#trashForm').submit();
    }  
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist2/css/lightbox.min.css">
<script src="<?php echo base_url(); ?>assets/dist2/js/lightbox-plus-jquery.min.js"></script>