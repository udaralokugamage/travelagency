<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add Package</h1>
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                       <div class="form-group required"> 
                             <label class="control-label">Package Name:</label>
                             <input class="form-control" type="text" name="package_name" />
                             <?php echo form_error('package_name'); ?>
                        </div>
                        <div class="form-group required"> 
                            <label class="control-label">Number of days:</label>
                             <input class="form-control" type="text" name="fixed_no_of_days" />
                            <?php echo form_error('fixed_no_of_days'); ?>
                       </div>                        
                        <div class="form-group required"> 
                            <label class="control-label">Description:</label>
                            <textarea class="form-control" name="description"></textarea>
                            <?php echo form_error('description'); ?>
                       </div>
                       <div class="form-group required"> 
                           <label class="control-label">Detailed Itinerary:</label> 
                            <textarea class="form-control" name="detailed_itinerary"></textarea> 
                       </div>
                       <div class="form-group required"> 
                           <label class="control-label">Price per adult:</label> 
                           <div class="input-group">
                               <span class="input-group-addon">$</span>                           
                               <input type="text" class="form-control" name="price_adult"/>
                           </div>   
                           <?php echo form_error('price_adult'); ?>
                       </div>
                       <div class="form-group"> 
                           <label>price per child:</label> 
                           <div class="input-group">
                               <span class="input-group-addon">$</span>                           
                               <input type="text" class="form-control" name="price_child"/>
                           </div>    
                           <?php echo form_error('price_child'); ?>
                       </div>                        
                       <div class="form-group required"> 
                           <label class="control-label">Region:</label> 
                           <select class="form-control" id="region_id" name="region_id">
                                <option value="">Select a Region</option>
                                <?php if(!empty($regions)){ ?>
                                    <?php foreach($regions as $region){ ?>
                                         <option value="<?php echo $region->id; ?>"><?php echo $region->r_name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                       </div>
                        <i id="loading" class="fa fa-spinner fa-spin" style="font-size:24px; color: #337AB7; display: none;"></i>
                        <div class="form-group required" id="subbox"> 
                           <label class="control-label">Country:</label> 
                           <select class="form-control" id="country_id" name="country_id">
                                <option value="">Select a Country</option>
                            </select>
                       </div>                        
                       <div class="form-group">
                           <label>Thumbnail Image<i>(For view all packages page)</i></label>
                           <input type="file" name="thumb_image"/>
                        </div>
                       <div class="form-group">
                           <label>Banner Image<i>(For package detail page)</i></label>
                           <input type="file" name="banner_image"/>
                        </div>                        
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add Package"/>
                        </div>
                       
                       
                   </form>                   
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>


<!-- /#page-wrapper -->
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script>
$("#region_id").change(function(){
    $('#loading').show();
    var id = $("#region_id").val();
    $.post("<?php echo base_url() ?>package/countries", {sub_id: id}, function(result){
        $("#subbox").html(result);
        $('#loading').hide();
    });
});
$('body').on('click','#country_id',function(){
    var id = $("#region_id").val();
    if(!id){
       alert('Please select a region first to retrieve its related contries.'); 
    }
    id = "";
});
</script>
