<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Users</h1>
            </div>
        </div>
                <div class="col-lg-12">
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <?php if($user_type == 1){ ?>
                                            <th>Status</th>
                                            <th class="sorter-false">&nbsp;</th>
                                            <th class="sorter-false">&nbsp;</th>
                                        <?php } ?>    
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($allData)){ $i = 1; ?>
                                    <?php foreach($allData as $data){ ?>
                                        <?php 
                                            if($i%2 == 1){
                                                $cl = "odd";
                                            }else{
                                                $cl = "even";
                                            }
                                        ?>
                                        <tr class="<?php echo $cl ?> gradeX">
                                            <td><?php echo $data->first_name; ?></td>
                                            <td><?php echo $data->last_name; ?></td>
                                            <td><?php echo $data->user_email; ?></td>
                                            <td><?php echo $userTypes[$data->user_type]; ?></td>
                                            <?php if($user_type == 1){ ?>
                                            <td class="center">
                                                <?php if($data->user_id != 1){ ?>
                                                    <?php 
                                                        if($data->user_status == 1){ 
                                                            $check = ' checked="" ';
                                                        }else{
                                                            $check = '';
                                                        }
                                                    ?>
                                                    <input <?php echo $check; ?> type="checkbox" class="statustrack" value="<?php echo $data->user_id; ?>" data-toggle="toggle" data-on="Enabled" data-off="Disabled">
                                                <?php } ?>
                                            </td> 
                                            <td class="center">
                                                <a href="<?php echo base_url() ?>user/edit?id=<?php echo $data->user_id; ?>">
                                                  <span class="glyphicon glyphicon-pencil"></span>
                                                </a>                                               
                                            </td>
                                            <td class="center">
                                                <?php if($data->user_id != 1){ ?>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="<?php echo $data->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Are you sure? the record will be permanently deleted
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <button type="button" class="btn btn-danger" onclick="deleteRecord('<?php echo $data->user_id; ?>')">Delete</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>                                                
                                                    <a data-toggle="modal" data-target="#<?php echo $data->user_id; ?>"  style="cursor: pointer;">
                                                      <span class="glyphicon glyphicon-remove"></span>
                                                    </a>   
                                                <?php } ?>                                             
                                            </td>
                                            <?php } ?> 
                                        </tr>
                                    <?php $i++; } ?>
                                <?php } ?>    
                                 </tbody>
                            </table>
                        </div>            
            <!-- /.col-lg-12 -->
                </div>
           </div>
         </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    jQuery.noConflict();
        jQuery('#dataTables-example').DataTable({
            responsive: true,
            <?php if($user_type == 1){ ?>
            columnDefs: [
               { orderable: false, targets: [-1,-2] }
            ]      
            <?php } ?>
        });

        function deleteRecord(id){
            window.location = '<?php echo base_url() ?>user/delete?id='+id;
        }          
        
        jQuery('.statustrack').change(function() {
           var id = $(this).val();
           if($(this).prop('checked')){
                $.post("<?php echo base_url() ?>user/status?st=1", {user_id: id}, function(result){
                });               
           }else{
                $.post("<?php echo base_url() ?>user/status?st=0", {user_id: id}, function(result){
                });                 
           }
        });
    </script>
<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script>

</script>