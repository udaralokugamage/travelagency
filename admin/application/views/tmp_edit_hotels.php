<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Edit Hotel</h1>
                    
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                
                    <form action="" method="post">
                       <div class="form-group required"> 
                             <label class="control-label">Hotel name:</label>
                             <input class="form-control" type="text" name="hotel_name" value="<?php echo $hotelData[0]->hotel_name ?>" />
                             <?php echo form_error('hotel_name'); ?>
                        </div>
                       <div class="form-group"> 
                             <label>Description:</label>
                             <input class="form-control" type="text" name="hotel_description" value="<?php echo $hotelData[0]->hotel_description ?>" />
                             <?php echo form_error('hotel_description'); ?>
                        </div>
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
