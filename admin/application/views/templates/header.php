<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url() ?>dashboard">Traveller Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><i class="fa fa-gear fa-fw"></i> <?php echo $first_name." ".$last_name; ?>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url() ?>dashboard/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo base_url() ?>dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i> Users
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url() ?>user">View Users</a>
                                    </li>      
                                <?php if($_SESSION['user_type'] == 1){ ?>    
                                    <li>
                                        <a href="<?php echo base_url() ?>user/add">Add Users</a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a href="<?php echo base_url() ?>user/changePassword">Change Password</a>
                                </li>                                
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Customers
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>customer">View Customers</a>
                                </li>                                
                            </ul>
                        </li>  
                        
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Regions
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>region">View Regions</a>
                                </li>    
                                <?php if($_SESSION['user_type'] == 1){ ?>
                                <li>
                                    <a href="<?php echo base_url() ?>region/add">Add Region</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>                          
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Country
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>country">View Country</a>
                                </li>    
                                <?php if($_SESSION['user_type'] == 1){ ?>
                                <li>
                                    <a href="<?php echo base_url() ?>country/add">Add Country</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>                          
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Airlines
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>airline">View Airlines</a>
                                </li>    
                                <?php if($_SESSION['user_type'] == 1){ ?>
                                <li>
                                    <a href="<?php echo base_url() ?>airline/add">Add Airline</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>  
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Hotels
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>hotel">View Hotels</a>
                                </li> 
                                <?php if($_SESSION['user_type'] == 1){ ?>
                                <li>
                                    <a href="<?php echo base_url() ?>hotel/add">Add Hotel</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>hotel/gallery">Photo Gallery</a>
                                </li>                                 
                                <?php } ?>
                            </ul>
                        </li>        
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Packages
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>package">View Packages</a>
                                </li>                                 
                                <li>
                                    <a href="<?php echo base_url() ?>package/add">Add Package</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>package/gallery">Photo Gallery</a>
                                </li> 
                                <?php if($_SESSION['user_type'] == 1){ ?>
                                <li>
                                    <a href="<?php echo base_url() ?>package/approve">Approve</a>
                                </li>  
                                <?php } ?>
                            </ul>
                        </li> 
                        <li>
                            <a href="#">
                                <i class="fa fa-files-o fa-fw"></i>Bookings
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url() ?>order">View Bookings</a>
                                </li>                                 
                            </ul>
                        </li>                         
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>