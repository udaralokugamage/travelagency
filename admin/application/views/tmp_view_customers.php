<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Customers</h1>
            </div>
        </div>
                <div class="col-lg-12">
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <?php if($user_type == 1){ ?>
                                            <th>Status</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($allData)){ $i = 1; ?>
                                    <?php foreach($allData as $data){ ?>
                                        <?php 
                                            if($i%2 == 1){
                                                $cl = "odd";
                                            }else{
                                                $cl = "even";
                                            }
                                        ?>
                                        <tr class="<?php echo $cl ?> gradeX">
                                            <td><?php echo $data->fname." ".$data->lname; ?></td>
                                            <td><?php echo $data->email; ?></td>
                                            <?php if($user_type == 1){ ?>
                                            <td class="center">
                                                <?php 
                                                    if($data->customer_status == 1){ 
                                                        $check = ' checked="" ';
                                                    }else{
                                                        $check = '';
                                                    }
                                                ?>
                                                <input <?php echo $check; ?> type="checkbox" class="statustrack" value="<?php echo $data->id; ?>" data-toggle="toggle" data-on="Enabled" data-off="Disabled">
                                            </td>
                                            <?php } ?> 
                                        </tr>
                                    <?php $i++; } ?>
                                <?php } ?>    
                                 </tbody>
                            </table>
                        </div>            
            <!-- /.col-lg-12 -->
                </div>
           </div>
         </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
</div>


<!-- /#page-wrapper -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    jQuery.noConflict();
        jQuery('#dataTables-example').DataTable({
            responsive: true,
            columnDefs: [
               { orderable: false, targets: [-1,-2] }
            ]            
        });
        jQuery('.statustrack').change(function() {
           var id = $(this).val();
           if($(this).prop('checked')){
                $.post("<?php echo base_url() ?>customer/status?st=1", {cus_id: id}, function(result){
                });               
           }else{
                $.post("<?php echo base_url() ?>customer/status?st=0", {cus_id: id}, function(result){
                });                 
           }
        });
    </script>
<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script>

</script>
